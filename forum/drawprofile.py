from io import BytesIO
from pathlib import Path

import aiohttp
import asyncio
import discord
from PIL import Image, ImageDraw, ImageFont


async def get_avatar(url: discord.Asset):
    try:
        async with aiohttp.ClientSession() as session:
            async with session.get(url._url) as http:
                img = await http.content.read()
                return BytesIO(img)
    except:
        return Path(__file__).parent / "error.png"


async def draw_profile(avatar_url: discord.Asset, nickname: str, username: str, rank: int, level: int,
                       percent: float, str_exp: str, str_needed: str, color: discord.Color):
    """
    :param avatar_url: can be reached for example via await user.avatar_url_as(format="png", size=1024)
    """
    color = color.to_rgb()

    image_mask = Path(__file__).parent / "mask.png"
    fontpath = Path(__file__).parent / "Roboto-Regular.ttf"

    mask = Image.open(image_mask)
    profile = Image.new("RGBA", mask.size, (0, 0, 0, 0))

    avatar_data = await get_avatar(avatar_url)
    avatar = Image.open(avatar_data)
    avatar = avatar.resize((138, 138), Image.BICUBIC)

    offset = (35, 53)
    profile.paste(avatar, offset)

    draw = ImageDraw.Draw(profile)

    pregion = [220, 157, 762, 188]
    pregion[2] = pregion[2] - (pregion[2] - pregion[0]) * (1 - percent)

    draw.rectangle(pregion, fill=color)
    profile.paste(mask, (0, 0), mask)

    small_size = 20
    medium_size = 35
    large_size = 50
    small = ImageFont.truetype(str(fontpath), small_size)
    medium = ImageFont.truetype(str(fontpath), medium_size)
    large = ImageFont.truetype(str(fontpath), large_size)

    x = 230
    draw.text((x, 150 - medium_size), nickname, "white", medium)
    if username != "":
        x = x + draw.textsize(nickname, medium)[0] + 10
        draw.text((x, 150 - small_size), f"({username})", "grey", small)

    str_needed = f"/{str_needed} XP"
    x = 752 - draw.textsize(str_needed, small)[0]
    draw.text((x, 150 - small_size), str_needed, "grey", small)
    x = x - draw.textsize(str_exp, small)[0] - 2
    draw.text((x, 150 - small_size), str_exp, "white", small)

    str_level = f"{level}"
    x = 750 - draw.textsize(str_level, large)[0]
    draw.text((x, 90 - large_size), str_level, color, large)
    x = x - draw.textsize("LEVEL", small)[0]
    draw.text((x, 90 - small_size), "LEVEL", color, small)

    str_rank = f"#{rank}"
    x = x - draw.textsize(str_rank, large)[0] - 10
    draw.text((x, 90 - large_size), str_rank, "white", large)
    x = x - draw.textsize("RANK", small)[0] - 5
    draw.text((x, 90 - small_size), "RANK", "white", small)

    profile.thumbnail((mask.size[0] / 2, mask.size[1] / 2))
    return profile


if __name__ == "__main__":
    loop = asyncio.get_event_loop()


    async def display():
        p = await draw_profile(
            "https://cdn.discordapp.com/avatars/106486455060176896/61abeeafde2cb2afee9d588ac778d6a2.png?size=128",
            "Bubňa", "tombuben", 4, 10, 1.23 / 2.31, "1.23K", "2.31K", "violet")
        p.show()

    loop.run_until_complete(display())
    loop.close()
