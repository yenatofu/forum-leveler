from .forum import Forum

def setup(bot):
    forum = Forum(bot)

    bot.add_listener(forum.listener, "on_message")
    bot.add_cog(forum)
